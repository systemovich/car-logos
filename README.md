# Car Logos

Web scraper for fetching logos of car manufacturers from http://www.carlogos.org.

## Installation

FIXME: explanation

## Usage

FIXME: explanation

    $ java -jar car-logos-0.1.0-standalone.jar [args]

## Options

FIXME: listing of options this app accepts.

## Examples

...

## Bugs

...


## License

Copyright (C) 2016 Geoffrey Bernardo van Wyk

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
