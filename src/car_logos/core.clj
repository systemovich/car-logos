(ns car-logos.core
  (:gen-class)
  (:require [org.httpkit.client :as http]
            [hickory.core :as hick]
            [hickory.select :as sel]
            [clojure.java.io :as io]
            [clojure.string :as string])
  (:import [java.net URL]))

(defn file-name
  "Creates file name prefixed with `directory` for image from `url`."
  ([url]
   (file-name url nil))
  ([url directory]
   (let [old (-> url URL. .getPath (string/split #"/") last)]
     (str (if directory
            directory
            (System/getProperty "user.dir"))
          "/"
          (string/replace old #"(-[a-z-]+-)|(-*[0-9x]+-*)" "")))))

(defn save-image!
  "Saves the given `images ` to disk. Each `image` is an HTML page in Hickory
  containing a single image only; no other content."
  ([image]
   (save-image! image nil))
  ([image directory]
  (with-open [in (io/input-stream (:body image))
              out (io/output-stream (file-name (-> image :opts :url)
                                               directory))]
    (io/copy in out))))

(defn save-images!
  "Saves the given `images ` to `directory` on disk. Each `image` is an
  HTML page in Hickory format containing a single image only; no other content."
  ([images]
   (save-images! images nil))
  ([images directory]
  (doseq [image images]
    (save-image! image directory))))

(defn fetch-images
  "Fetches all the image files at the given `urls`."
  [urls]
  (->> urls
       (map http/get)
       (map #(deref %))
       (filter #(= (:status %) 200))))

(defn extract-image-url
  "Extracts the link to the logo image from the `page`."
  [page]
  (-> (sel/select (sel/descendant (sel/class :main-l)
                                  (sel/class :content)
                                  (sel/and (sel/tag :p) (sel/class :art-img))
                                  (sel/tag :a))
                  page)
      first
      :attrs
      :href))

(defn extract-image-urls
  "Extracts the link to the logo image from each of the `pages`."
  [pages]
  (->> pages
       (map extract-image-url)
       (remove #(nil? %))))

(defn page-as-hickory
  "Converts an HTTP response into HTML in Hickory format."
  [response]
  (-> @response :body hick/parse hick/as-hickory))

(defn fetch-pages
  "Fetches all individual logo pages or images at the given `urls`."
  [urls]
  (->> urls
       (map http/get)
       (map page-as-hickory)))

(defn extract-page-urls
  "Extracts the links to the individual logo pages from the index page."
  [index]
  (remove #(nil? %)
          (map #(:href (:attrs %))
               (sel/select (sel/descendant (sel/and (sel/tag :div)
                                                    (sel/class :all-list))
                                           (sel/tag :a))
                           index))))

(defn fetch-index
  "Fetches the page containing the names of all car manufacturers. Each
  name is a link to the page containing the manufacturers logo. The HTML of the
  page is returned in Hickory format."
  []
  (page-as-hickory (http/get "http://www.carlogos.org/All-List/")))

(defn -main
  "Main entry point into program when executed from the command line. Takes as
  optional argument the path to the `directory` to which the logos should be
  saved. If no directory is provided, downloads the logos the current
  directory."
  ([]
   (-main nil))
  ([directory]
   (if (and (not (nil? directory)) (not (.isDirectory (io/file directory))))
     (println "The directory you provided does not exist: " directory)
     (save-images! (->  (fetch-index)
                        extract-page-urls
                        fetch-pages
                        extract-image-urls
                        fetch-images)
                   directory))))
