(defproject car-logos "0.1.0-SNAPSHOT"
  :description "Web scraper for fetching logos of car manufacturers from carlogos.org."
  :url "https://gitlab.com/systemovich/car-logos"
  :license {:name "GNU General Public License"
            :url "https://gnu.org/licenses/gpl-3.0.txt"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [http-kit "2.2.0"]
                 [hickory "0.7.0"]]
  :main ^:skip-aot car-logos.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
